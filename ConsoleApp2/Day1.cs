﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode2016
{
    class Day1
    {
        enum DIR
        {
            NORTH,
            EAST,
            SOUTH,
            WEST
        }

        //static void Main(string[] args) {
        //    string input = "R2, L3";
        //    System.Diagnostics.Debug.WriteLine(Distance(input));
        //    input = "R2, R2, R2";
        //    System.Diagnostics.Debug.WriteLine(Distance(input));
        //    input = "R5, L5, R5, R3";
        //    System.Diagnostics.Debug.WriteLine(Distance(input));
        //    input = "R2, L1, R2, R1, R1, L3, R3, L5, L5, L2, L1, R4, R1, R3, L5, L5, R3, L4, L4, R5, R4, R3, L1, L2, R5, R4, L2, R1, R4, R4, L2, L1, L1, R190, R3, L4, R52, R5, R3, L5, R3, R2, R1, L5, L5, L4, R2, L3, R3, L1, L3, R5, L3, L4, R3, R77, R3, L2, R189, R4, R2, L2, R2, L1, R5, R4, R4, R2, L2, L2, L5, L1, R1, R2, L3, L4, L5, R1, L1, L2, L2, R2, L3, R3, L4, L1, L5, L4, L4, R3, R5, L2, R4, R5, R3, L2, L2, L4, L2, R2, L5, L4, R3, R1, L2, R2, R4, L1, L4, L4, L2, R2, L4, L1, L1, R4, L1, L3, L2, L2, L5, R5, R2, R5, L1, L5, R2, R4, R4, L2, R5, L5, R5, R5, L4, R2, R1, R1, R3, L3, L3, L4, L3, L2, L2, L2, R2, L1, L3, R2, R5, R5, L4, R3, L3, L4, R2, L5, R5";
        //    System.Diagnostics.Debug.WriteLine(Distance(input));
        //}

        static int Distance(string input) {
            int xPos = 0, yPos = 0;
            DIR direction = DIR.NORTH;

            string[] inputArray = input.Split(',');
            for (int i = 0; i < inputArray.Length; i++) {
                string newInput = inputArray[i].Trim();
                char turn = newInput[0];
                int length = int.Parse(newInput.Substring(1));

                if (turn == 'R') {
                    switch (direction) {
                        case DIR.NORTH:
                            xPos += length;
                            direction = DIR.EAST;
                            break;
                        case DIR.EAST:
                            yPos -= length;
                            direction = DIR.SOUTH;
                            break;
                        case DIR.SOUTH:
                            xPos -= length;
                            direction = DIR.WEST;
                            break;
                        case DIR.WEST:
                            yPos += length;
                            direction = DIR.NORTH;
                            break;
                    }
                } else {
                    switch (direction) {
                        case DIR.NORTH:
                            xPos -= length;
                            direction = DIR.WEST;
                            break;
                        case DIR.EAST:
                            yPos += length;
                            direction = DIR.NORTH;
                            break;
                        case DIR.SOUTH:
                            xPos += length;
                            direction = DIR.EAST;
                            break;
                        case DIR.WEST:
                            yPos -= length;
                            direction = DIR.SOUTH;
                            break;
                    }
                }
            }

            return Math.Abs(xPos) + Math.Abs(yPos);
        }
    }
}
