﻿using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2017 {
	class Day18 {

		public static void Solve() {
			//RecoveredFrequency("..\\..\\Day18-Input Example.txt");
			//RecoveredFrequency("..\\..\\Day18-Input.txt");
			//DeadLock("..\\..\\Day18-Input Example2.txt");
			DeadLock("..\\..\\Day18-Input.txt");
		}

		private static long lastPlayedFrequency;

		private static void RecoveredFrequency(string @filePath) {
			List<Instruction> instructions = RetrieveInstructions(filePath);

			Dictionary<string, long> registers = new Dictionary<string, long>();
			int i = 0;
			while (true) {
				int indexOffset = ExecuteInstruction1(instructions[i], registers);
				if (indexOffset == int.MaxValue) {
					break;
				}
				i += indexOffset;
			}

			System.Diagnostics.Debug.WriteLine(lastPlayedFrequency);
		}

		private static int ExecuteInstruction1(Instruction instruction, Dictionary<string, long> registers) {
			int indexOffset = 1;
			switch (instruction.command) {
				case "snd":
					lastPlayedFrequency = GetValue(instruction.register, registers);
					break;
				case "set":
					SetValue(instruction.register, GetValue(instruction.value, registers), registers);
					break;
				case "add":
					SetValue(instruction.register, GetValue(instruction.register, registers) + GetValue(instruction.value, registers), registers);
					break;
				case "mul":
					SetValue(instruction.register, GetValue(instruction.register, registers) * GetValue(instruction.value, registers), registers);
					break;
				case "mod":
					SetValue(instruction.register, GetValue(instruction.register, registers) % GetValue(instruction.value, registers), registers);
					break;
				case "rcv":
					if (GetValue(instruction.register, registers) != 0) {
						indexOffset = int.MaxValue;
					}
					break;
				case "jgz":
					if (GetValue(instruction.register, registers) > 0) {
						indexOffset = (int)GetValue(instruction.value, registers);
					}
					break;
			}

			return indexOffset;
		}

		private static void DeadLock(string @filePath) {
			List<Instruction> instructions = RetrieveInstructions(filePath);

			Dictionary<string, long> registers0 = new Dictionary<string, long>();
			registers0.Add("p", 0);
			Dictionary<string, long> registers1 = new Dictionary<string, long>();
			registers1.Add("p", 1);

			Queue<long> queue0 = new Queue<long>();
			Queue<long> queue1 = new Queue<long>();

			int program1SendCount = 0;

			int i = 0, j = 0;
			while (!(instructions[i].command.Equals("rcv") && queue0.Count == 0
				&& instructions[j].command.Equals("rcv") && queue1.Count == 0)) {
				// Program0 
				while (true){
					int indexOffset = ExecuteInstruction2(instructions[i], registers0, queue0, queue1);
					if (indexOffset != int.MaxValue) {
						break;
					}
					i += indexOffset;
				}
				// Program1
				while (true) {
					if (instructions[j].command.Equals("snd")) {
						program1SendCount++;
					}
					int indexOffset = ExecuteInstruction2(instructions[j], registers1, queue1, queue0);
					if (indexOffset == int.MaxValue) {
						break;
					}
					j += indexOffset;
				}
			}

			System.Diagnostics.Debug.WriteLine(program1SendCount);
		}

		private static int ExecuteInstruction2(Instruction instruction, Dictionary<string, long> registers, Queue<long> myQueue, Queue<long> otherQueue) {
			int indexOffset = 1;
			switch (instruction.command) {
				case "snd":
					otherQueue.Enqueue(GetValue(instruction.register, registers));
					break;
				case "set":
					SetValue(instruction.register, GetValue(instruction.value, registers), registers);
					break;
				case "add":
					SetValue(instruction.register, GetValue(instruction.register, registers) + GetValue(instruction.value, registers), registers);
					break;
				case "mul":
					SetValue(instruction.register, GetValue(instruction.register, registers) * GetValue(instruction.value, registers), registers);
					break;
				case "mod":
					SetValue(instruction.register, GetValue(instruction.register, registers) % GetValue(instruction.value, registers), registers);
					break;
				case "rcv":
					if (myQueue.Count != 0) {
						SetValue(instruction.register, myQueue.Dequeue(), registers);
					} else {
						// Waiting
						indexOffset = int.MaxValue;
					}
					break;
				case "jgz":
					if (GetValue(instruction.register, registers) > 0) {
						indexOffset = (int)GetValue(instruction.value, registers);
					}
					break;
			}

			return indexOffset;
		}


		private static void SetValue(string register, long value, Dictionary<string, long> registers) {
			if (registers.ContainsKey(register)) {
				registers[register] = value;
			} else {
				registers.Add(register, value);
			}
		}

		private static long GetValue(string registerOrValue, Dictionary<string, long> registers) {
			long value;
			if (!long.TryParse(registerOrValue, out value)) {
				if (registers.ContainsKey(registerOrValue)) {
					value = registers[registerOrValue];
				} else {
					value = 0;
					registers.Add(registerOrValue, 0);
				}
			}

			return value;
		}

		private static List<Instruction> RetrieveInstructions(string filePath) {
			StreamReader file = new StreamReader(@filePath);

			List<Instruction> instructions = new List<Instruction>();

			string line;
			while ((line = file.ReadLine()) != null) {
				string[] lineSplit = line.Split();
				Instruction instruction = new Instruction();
				instruction.command = lineSplit[0];
				instruction.register = lineSplit[1];
				if (lineSplit.Length > 2) {
					instruction.value = lineSplit[2];
				}
				instructions.Add(instruction);
			}

			return instructions;
		}

		private struct Instruction {
			public string command;
			public string register;
			public string value;
		}
	}
}