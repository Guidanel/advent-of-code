﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode2017
{
    class Day4
    {
        public static void Solve() {
			System.Diagnostics.Debug.WriteLine(Valid("..\\..\\Day4-Input.txt"));
		}

		static int Valid(string filePath) { 
			StreamReader file = new StreamReader(@filePath);

			string line;
			int valid = 0;

			while ((line = file.ReadLine()) != null) {
				if (IsLineValid(line)) {
					valid++;
				}
			}

			return valid;
		}

		static bool IsLineValid(string line) {
			string[] words = line.Split();
			int length = words.Length;

			// For part 2
			for (int i = 0; i < length; i++) {
				words[i] = String.Concat(words[i].OrderBy(c => c));
			}
			// End for part 2

			List<string> alreadySeen = new List<string>();
			bool isValid = true;

			for (int i = 0; i < length; i++) {
				string word = words[i];
				if (alreadySeen.Contains(word)) {
					// not valid
					isValid = false;
					break;
				} else {
					alreadySeen.Add(word);
				}
			}

			return isValid;
		}
	}
}
