﻿using System.IO;

namespace AdventOfCode2017
{
    class Day9 {

		public static void Solve() {
            //Score("..\\..\\Day9-Input Example.txt");
            //Score("..\\..\\Day9-Input Example2.txt");
            Score("..\\..\\Day9-Input.txt");
		}

        private static void Score(string @filePath) {
            StreamReader file = new StreamReader(@filePath);

            string line;
            while ((line = file.ReadLine()) != null) {
                LineScore(line);
            }
        }

        private static void LineScore(string line) {
            int score = 0;
            int garbage = 0;
            int depth = 0;
            bool consumingGarbage = false;
            for (int i = 0; i < line.Length; i++) {
                switch (line[i]) {
                    case '<':
                        if (!consumingGarbage) {
                            // garbage
                            consumingGarbage = true;
                        } else {
                            garbage++;
                        }
                        break;
                    case '>':
                        // end of garbage
                        consumingGarbage = false;
                        break;
                    case '{':
                        if (!consumingGarbage) {
                            // new group
                            depth++;
                            score += depth;
                        } else {
                            garbage++;
                        }
                        break;
                    case '}':
                        if (!consumingGarbage) {
                            // end of group
                            depth--;
                        } else {
                            garbage++;
                        }
                        break;
                    case '!':
                        i++;
                        break;
                    default:
                        if (consumingGarbage) {
                            garbage++;
                        }
                        break;
                }
            }

            //System.Diagnostics.Debug.WriteLine("Score " + score);
            System.Diagnostics.Debug.WriteLine("Garbage " + garbage);
        }
    }
}