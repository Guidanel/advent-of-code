﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2017 {
    class Day25
    {

        public static void Solve() {
            //Turing("..\\..\\Day25-Input Example.txt");
            Turing("..\\..\\Day25-Input.txt");
        }

        private static char startState;
        private static int stepsNumber;
        private static Dictionary<char, Instructions> turingMachine = new Dictionary<char, Instructions>();

        private static int currentPosition;
        private static char currentState;
        private static List<int> values = new List<int> { 0 };

        private static void Turing(string @filePath) {
            ReadInput(filePath);

            currentState = startState;
            while (stepsNumber > 0) {
                ExecuteStateInstructions();
                stepsNumber--;
            }

            int checkSum = 0;
            for (int i = 0; i < values.Count; i++) {
                checkSum += values[i];
            }
            System.Diagnostics.Debug.WriteLine(checkSum);
        }

        private static void ExecuteStateInstructions() {
            List<Instruction> instructions = values[currentPosition] == 0 ? turingMachine[currentState].zeroInstructions : turingMachine[currentState].oneInstructions;
            for (int i = 0; i < instructions.Count; i++) {
                Instruction instruction = instructions[i];
                switch (instruction.command) {
                    case Instruction.WRITE:
                        values[currentPosition] = int.Parse(instruction.argument);
                        break;
                    case Instruction.MOVE:
                        if (instruction.argument == Instruction.RIGHT) {
                            if (currentPosition == values.Count - 1) {
                                values.Add(0);
                            }
                            currentPosition++;
                        } else { // Left
                            if (currentPosition == 0) {
                                values.Insert(0, 0);
                            } else {
                                currentPosition--;
                            }
                        }
                        break;
                    case Instruction.CONTINUE:
                        currentState = instruction.argument[0];
                        break;
                }
            }
        }

        private static void ReadInput(string filePath) {
            StreamReader file = new StreamReader(@filePath);

            string line;
            
            // Start state
            line = file.ReadLine();
            startState = line[line.Length - 2];

            // Steps number
            line = file.ReadLine();
            string[] lineSplit = Regex.Matches(line, @"\d+").Cast<Match>().Select(m => m.Value).ToArray();
            stepsNumber = int.Parse(lineSplit[0]);
            
            while ((line = file.ReadLine()) != null) {
                ReadState(file);
            }
        }

        private static void ReadState(StreamReader file) {
            // State name
            string line = file.ReadLine();
            char stateName = line[line.Length - 2];

            // If the current value is 0:
            file.ReadLine();

            Instructions instructions = new Instructions();
            instructions.zeroInstructions = ReadInstructions(file);
            file.ReadLine();
            instructions.oneInstructions = ReadInstructions(file);

            turingMachine.Add(stateName, instructions);
        }

        private static List<Instruction> ReadInstructions(StreamReader file) {
            List<Instruction> instructions = new List<Instruction>();

            for (int i = 0; i < 3; i++) {
                Instruction instruction = new Instruction();
                string line = file.ReadLine();
                if (line.Contains("Write")) {
                    instruction.command = Instruction.WRITE;
                    instruction.argument = line[line.Length - 2].ToString();
                } else if (line.Contains("Move")) {
                    instruction.command = Instruction.MOVE;
                    instruction.argument = line.Contains("left") ? Instruction.LEFT : Instruction.RIGHT;
                } else { // Continue
                    instruction.command = Instruction.CONTINUE;
                    instruction.argument = line[line.Length - 2].ToString();
                }
                instructions.Add(instruction);
            }

            return instructions;
        }
    }

    internal class Instructions
    {
        public List<Instruction> zeroInstructions = new List<Instruction>();
        public List<Instruction> oneInstructions = new List<Instruction>();
    }

    internal class Instruction
    {
        public const string WRITE = "Write";
        public const string MOVE = "Move";
        public const string CONTINUE = "Continue";
        public const string LEFT = "Left";
        public const string RIGHT = "Right";

        public string command;
        public string argument;
    }
}