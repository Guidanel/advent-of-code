﻿namespace AdventOfCode2017
{
    class Day15 {

		public static void Solve() {
			//JudgeCount(277, 349);
			JudgeCount2(277, 349);
		}

		private static void JudgeCount(long valueA, long valueB) {
			int judgeCount = 0;
			for (int i = 0; i < 40000000; i++) {
				valueA = (valueA * 16807) % 2147483647;
				valueB = (valueB * 48271) % 2147483647;
				if (Last16bitsEqual(valueA, valueB)) {
					judgeCount++;
				}
			}

			System.Diagnostics.Debug.WriteLine(judgeCount);
		}

		private static void JudgeCount2(long valueA, long valueB) {
			int judgeCount = 0;
			for (int i = 0; i < 5000000; i++) {
				NextValueA(ref valueA);
				NextValueB(ref valueB);
				if (Last16bitsEqual(valueA, valueB)) {
					judgeCount++;
				}
			}

			System.Diagnostics.Debug.WriteLine(judgeCount);
		}

		private static bool Last16bitsEqual(long valueA, long valueB) {
			int mask = 65535;
			return (valueA & mask) == (valueB & mask);
		}

		private static void NextValueA(ref long valueA) {
			do {
				valueA = (valueA * 16807) % 2147483647;
			} while (valueA % 4 != 0);
		}

		private static void NextValueB(ref long valueB) {
			do {
				valueB = (valueB * 48271) % 2147483647;
			} while (valueB % 8 != 0);
		}
	}
}