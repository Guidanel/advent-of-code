﻿using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode2017
{
    class Day16 {

		public static void Solve() {
			//Dance("abcde", "..\\..\\Day16-Input Example.txt");
			Dance("abcdefghijklmnop", "..\\..\\Day16-Input.txt");
		}

		private static void Dance(string input, string @filePath) {
			StreamReader file = new StreamReader(@filePath);
			string line = file.ReadLine();

			string[] instructions = line.Split(new char[] { ',' });
			StringBuilder programs = new StringBuilder(input);

			int cycleLength = FindCycleLength(input, ref instructions, programs);

			if (cycleLength != 0) {
				int necessaryCycles = 1000000000 % (cycleLength + 1);
				for (int j = 0; j < necessaryCycles; j++) {
					for (int i = 0; i < instructions.Length; i++) {
						ExecuteInstruction(instructions[i], programs);
					}
				}
			}

			System.Diagnostics.Debug.WriteLine(programs.ToString());
		}

		private static int FindCycleLength(string input, ref string[] instructions, StringBuilder programs) {
			int cycleLength = 0;
			for (int j = 0; j < 1000000000; j++) {
				for (int i = 0; i < instructions.Length; i++) {
					ExecuteInstruction(instructions[i], programs);
				}

				if (programs.ToString().Equals(input)) {
					cycleLength = j;
					break;
				}
			}

			return cycleLength;
		}

		private static void ExecuteInstruction(string instruction, StringBuilder programs) {
			switch (instruction[0]) {
				case 's':
					string[] instructionSplit = Regex.Matches(instruction, @"\d+").Cast<Match>().Select(m => m.Value).ToArray();
					Spin(int.Parse(instructionSplit[0]), programs); 
					break;
				case 'x':
					string[] instructionSplit2 = Regex.Matches(instruction, @"\d+").Cast<Match>().Select(m => m.Value).ToArray();
					Swap(int.Parse(instructionSplit2[0]), int.Parse(instructionSplit2[1]), programs);
					break;
				case 'p':
					string[] instructionSplit3 = Regex.Matches(instruction.Substring(1), @"\w+").Cast<Match>().Select(m => m.Value).ToArray();
					Swap(programs.ToString().IndexOf(instructionSplit3[0]), programs.ToString().IndexOf(instructionSplit3[1]), programs);
					break;
			}
		}

		private static void Swap(int i, int j, StringBuilder programs) {
			char temp = programs[i];
			programs[i] = programs[j];
			programs[j] = temp;
		}

		private static void Spin(int spinLength, StringBuilder programs) {
			int programsLength = programs.Length;
			programs.Insert(0, programs.ToString().Substring(programsLength - spinLength));
			programs.Remove(programsLength, spinLength);
		}
	}
}