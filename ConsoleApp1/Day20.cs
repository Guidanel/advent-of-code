﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2017
{
    class Day20 {

		public static void Solve() {
            ClosestParticle("..\\..\\Day20-Input.txt");
        }

        private static void ClosestParticle(string @filePath) {
			StreamReader file = new StreamReader(@filePath);

            List<Particle> particles = new List<Particle>();

			string line;
			while ((line = file.ReadLine()) != null) {
                particles.Add(new Particle(line));
            }

            Particle closestParticle = null;
            int timesBeingTheClosest = 0;

            while (timesBeingTheClosest < 500) {
                Particle currentClosestParticle = MoveAndFindClosestParticle(particles);
                if (currentClosestParticle == closestParticle) {
                    timesBeingTheClosest++;
                } else {
                    timesBeingTheClosest = 1;
                    closestParticle = currentClosestParticle;
                }

                // Part 2
                HandleCollisions(particles);
            }

            // Part 1
            //System.Diagnostics.Debug.WriteLine(particles.IndexOf(closestParticle));
            // Part 2
            System.Diagnostics.Debug.WriteLine(particles.Count);
        }

        private static void HandleCollisions(List<Particle> particles) {
            List<Particle> collidingParticles = new List<Particle>();
            for (int i = 0; i < particles.Count; i++) {
                Particle firstParticle = particles[i];
                for (int j = i + 1; j < particles.Count; j++) {
                    Particle secondParticle = particles[j];
                    if (firstParticle.CollidesWith(secondParticle)) {
                        if (!collidingParticles.Contains(firstParticle)) {
                            collidingParticles.Add(firstParticle);
                        }
                        if (!collidingParticles.Contains(secondParticle)) {
                            collidingParticles.Add(secondParticle);
                        }
                    }
                }
            }

            for (int i = 0; i < collidingParticles.Count; i++) {
                particles.Remove(collidingParticles[i]);
            }
        }

        private static Particle MoveAndFindClosestParticle(List<Particle> particles) {
            Particle closestParticle = null;
            int closestDistance = int.MaxValue;
            for (int i = 0; i < particles.Count; i++) {
                particles[i].Move();
                int distance = particles[i].Distance();
                if (distance < closestDistance) {
                    closestDistance = distance;
                    closestParticle = particles[i];
                }
            }

            return closestParticle;
        }
    }

    class Particle
    {
        int[] position = new int[3];
        int[] velocity = new int[3];
        int[] acceleration = new int[3];

        public Particle(string particleDescriptionString) {
            string[] lineSplit = Regex.Matches(particleDescriptionString, @"-?\d+").Cast<Match>().Select(m => m.Value).ToArray();
            position[0] = int.Parse(lineSplit[0]);
            position[1] = int.Parse(lineSplit[1]);
            position[2] = int.Parse(lineSplit[2]);
            velocity[0] = int.Parse(lineSplit[3]);
            velocity[1] = int.Parse(lineSplit[4]);
            velocity[2] = int.Parse(lineSplit[5]);
            acceleration[0] = int.Parse(lineSplit[6]);
            acceleration[1] = int.Parse(lineSplit[7]);
            acceleration[2] = int.Parse(lineSplit[8]);
        }

        public void Move() {
            for (int i = 0; i < velocity.Length; i++) {
                velocity[i] += acceleration[i];
                position[i] += velocity[i];
            }
        }

        public int Distance() {
            return Math.Abs(position[0]) + Math.Abs(position[1]) + Math.Abs(position[2]);
        }

        public bool CollidesWith(Particle other) {
            return position[0] == other.position[0]
                && position[1] == other.position[1]
                && position[2] == other.position[2];
        }
    }
}