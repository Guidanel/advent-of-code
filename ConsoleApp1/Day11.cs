﻿using System;
using System.IO;

namespace AdventOfCode2017
{
    class Day11
    {

        public static void Solve() {
            //FileDistance("..\\..\\Day11-Input Example.txt");
            FileDistance("..\\..\\Day11-Input.txt");
        }

        private static void FileDistance(string @filePath) {
            StreamReader file = new StreamReader(@filePath);

            string line;
            while ((line = file.ReadLine()) != null) {
                LineDistance(line);
            }
        }

        private static void LineDistance(string line) {
            int x = 0;
            int y = 0;
            int z = 0;
            int maxDistance = 0;

            string[] inputs = line.Split(new char[] { ',' });
            for (int i = 0; i < inputs.Length; i++) {
                switch (inputs[i]) {
                    case "n":
                        y++;
                        z--;
                        break;
                    case "ne":
                        x++;
                        z--;
                        break;
                    case "se":
                        x++;
                        y--;
                        break;
                    case "s":
                        y--;
                        z++;
                        break;
                    case "sw":
                        x--;
                        z++;
                        break;
                    case "nw":
                        x--;
                        y++;
                        break;
                }

                int currentDistance = Distance(x, y, z);
                if (currentDistance > maxDistance) {
                    maxDistance = currentDistance;
                }
            }

            System.Diagnostics.Debug.WriteLine("Distance " + Distance(x, y, z));
            System.Diagnostics.Debug.WriteLine("Max " + maxDistance);
        }

        private static int Distance(int x, int y, int z) {
            return Math.Max(Math.Abs(x), Math.Max(Math.Abs(y), Math.Abs(z)));
        }
    }
}