﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2017
{
    class Day13
    {

        public static void Solve() {
            //Severity("..\\..\\Day13-Input Example.txt");
            Severity("..\\..\\Day13-Input.txt");
        }

        private static void Severity(string @filePath) {
            StreamReader file = new StreamReader(@filePath);

            Dictionary<int, int> firewall = new Dictionary<int, int>();

            string line;
            while ((line = file.ReadLine()) != null) {
                string[] lineSplit = Regex.Matches(line, @"\d+").Cast<Match>().Select(m => m.Value).ToArray();
                firewall.Add(int.Parse(lineSplit[0]), int.Parse(lineSplit[1]));
            }

            // Part 1
            //int severity;
            //GotCaught(firewall, out severity, 0);
            //System.Diagnostics.Debug.WriteLine(severity);

            // Part 2
            bool gotCaught = true;
            int severity;
            int delay = 0;
            while (gotCaught) {
                delay++;
                gotCaught = GotCaught(firewall, out severity, delay);
            }
            System.Diagnostics.Debug.WriteLine(delay);
        }

        private static bool GotCaught(Dictionary<int, int> firewall, out int severity, int delay) {
            severity = 0;
            bool gotCaught = false;
            foreach (var item in firewall) {
                int cycleLength = (item.Value - 1) * 2;
                bool caught = (item.Key + delay) % cycleLength == 0;
                if (caught) {
                    severity += item.Key * item.Value;
                    gotCaught = true;
                }
            }
            
            return gotCaught;
        }
    }
}