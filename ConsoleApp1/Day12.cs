﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2017
{
    class Day12
    {

        public static void Solve() {
            //ProgramsCount("..\\..\\Day12-Input Example.txt");
            ProgramsCount("..\\..\\Day12-Input.txt");
        }

        private static List<List<int>> connectionsList = new List<List<int>>();

        private static void ProgramsCount(string @filePath) {
            StreamReader file = new StreamReader(@filePath);
            
            string line;
            while ((line = file.ReadLine()) != null) {
                string[] lineSplit = Regex.Matches(line, @"\d+").Cast<Match>().Select(m => m.Value).ToArray();
                List<int> connections = new List<int>();
                for (int i = 1; i < lineSplit.Length; i++) {
                    connections.Add(int.Parse(lineSplit[i]));
                }

                connectionsList.Add(connections);
            }

            // Part 1
            //AddPrograms(0, new List<int>());
            //System.Diagnostics.Debug.WriteLine(group.Count);

            // Part 2
            int groupNumber = 0;
            for (int i = 0; i < connectionsList.Count; i++) {
                List<int> connections = connectionsList[i];
                if (connections.Count > 0) {
                    AddPrograms(i, new List<int>());
                    groupNumber++;
                }
            }
            System.Diagnostics.Debug.WriteLine(groupNumber);
        }

        private static void AddPrograms(int index, List<int> group) {
            group.Add(index);

            List<int> connections = connectionsList[index];
            List<int> connectionsToExplore = new List<int>();
            for (int i = 0; i < connections.Count; i++) {
                if (!group.Contains(connections[i])) {
                    connectionsToExplore.Add(connections[i]);
                }
            }

            connections.Clear();

            for (int i = 0; i < connectionsToExplore.Count; i++) {
                int indexToExplore = connectionsToExplore[i];
                // We need to check again as it could have been added by the recursive call below
                if (!group.Contains(indexToExplore)) {
                    AddPrograms(indexToExplore, group);
                }
            }
        }
    }
}