﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode2017
{
    class Day22
    {

        public static void Solve() {
            //Virus("..\\..\\Day22-Input Example.txt", 10000000);
            Virus("..\\..\\Day22-Input.txt", 10000000);
        }

        private static List<StringBuilder> nodes = new List<StringBuilder>();
        private static int posX, posY;
        private static int width, height;
        private static int infectionsCount;

        private const int UP = 0;
        private const int RIGHT = 1;
        private const int DOWN = 2;
        private const int LEFT = 3;
        private static int direction = UP;

        private const char INFECTED = '#';
        private const char CLEAN = '.';
        private const char WEAKENED = 'W';
        private const char FLAGGED = 'F';

        private static void Virus(string @filePath, int iterations) {
            StreamReader file = new StreamReader(@filePath);

            string line;
            while ((line = file.ReadLine()) != null) {
                nodes.Add(new StringBuilder(line));
            }

            width = nodes[0].Length;
            posX = width / 2;
            height = nodes.Count;
            posY = height / 2;

            while (iterations > 0) {
                Burst();
                iterations--;
            }

            System.Diagnostics.Debug.WriteLine(infectionsCount);
        }

        private static void Burst() {
            switch (GetState()) {
                case CLEAN: TurnLeft(); break;
                case INFECTED: TurnRight(); break;
                case FLAGGED: UTurn(); break;
            }

            // Part 1
            //ChangeState1();
            // Part 2
            ChangeState2();
            MoveForward();
        }

        private static void ChangeState2() {
            switch (GetState()) {
                case CLEAN: SetState(WEAKENED); break;
                case WEAKENED: SetState(INFECTED);
                    infectionsCount++; break;
                case INFECTED: SetState(FLAGGED); break;
                case FLAGGED: SetState(CLEAN); break;
            }
        }

        private static void ChangeState1() {
            switch (GetState()) {
                case CLEAN: SetState(INFECTED);
                    infectionsCount++; break;
                case INFECTED: SetState(CLEAN); break;
            }
        }

        private static char GetState() {
            return nodes[posY][posX];
        }

        private static void SetState(char state) {
            nodes[posY][posX] = state;
        }

        private static void TurnRight() {
            direction = (direction + 1) % 4;
        }

        private static void TurnLeft() {
            direction = (direction - 1 + 4) % 4;
        }

        private static void UTurn() {
            direction = (direction + 2) % 4;
        }

        private static void MoveForward() {
            switch (direction) {
                case UP:
                    if (posY > 0) {
                        posY--;
                    } else {
                        AddRow(UP);
                    }
                    break;
                case DOWN:
                    posY++;
                    if (posY == height) {
                        AddRow(DOWN);
                    }
                    break;
                case LEFT:
                    if (posX > 0) {
                        posX--;
                    } else {
                        AddColumn(LEFT);
                    }
                    break;
                case RIGHT:
                    posX++;
                    if (posX == width) {
                        AddColumn(RIGHT);
                    }
                    break;
            }
        }

        private static void AddColumn(int direction) {
            if (direction == LEFT) {
                for (int i = 0; i < nodes.Count; i++) {
                    nodes[i].Insert(0, CLEAN);
                }
            } else if (direction == RIGHT) {
                for (int i = 0; i < nodes.Count; i++) {
                    nodes[i].Append(CLEAN);
                }
            }
            width++;
        }

        private static void AddRow(int direction) {
            StringBuilder newRow = new StringBuilder();
            for (int i = 0; i < width; i++) {
                newRow.Append(CLEAN);
            }

            if (direction == UP) {
                nodes.Insert(0, newRow);
            } else if (direction == DOWN) {
                nodes.Add(newRow);
            }
            height++;
        }
    }
}