﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode2017
{
    class Day21 {

        public static void Solve() {
            //Enhance("..\\..\\Day21-Input Example.txt", 2);
            //Enhance("..\\..\\Day21-Input.txt", 5);
            Enhance("..\\..\\Day21-Input.txt", 18);
        }

        private static List<char[,]> input2x2Patterns = new List<char[,]>();
        private static List<char[,]> output3x3Patterns = new List<char[,]>();
        private static List<char[,]> input3x3Patterns = new List<char[,]>();
        private static List<char[,]> output4x4Patterns = new List<char[,]>();

        private static Dictionary<string, char[,]> patternsFound = new Dictionary<string, char[,]>();

        private static void Enhance(string @filePath, int iterations) {
			StreamReader file = new StreamReader(@filePath);
            
			string line;
			while ((line = file.ReadLine()) != null) {
                ReadInputOutputPatterns(line);
            }

            // .#.
            // ..#
            // ###
            char[,] currentMatrix = new char[,] { { '.', '#', '.' }, { '.', '.', '#' }, { '#', '#', '#' } };

            while (iterations > 0) {
                EnhanceMatrix(ref currentMatrix);
                iterations--;
            }
            
            System.Diagnostics.Debug.WriteLine(NumberOfPixelsOn(ref currentMatrix));
        }

        private static void EnhanceMatrix(ref char[,] currentMatrix) {
            List<char[,]> inputPatterns = input3x3Patterns;
            List<char[,]> outputPatterns = output4x4Patterns;
            int rowNumbers = currentMatrix.GetLength(0) / 3;
            int blockSize = 3;

            if (currentMatrix.Length % 2 == 0) {
                inputPatterns = input2x2Patterns;
                outputPatterns = output3x3Patterns;
                rowNumbers = currentMatrix.GetLength(0) / 2;
                blockSize = 2;
            }

            char[,] newMatrix = new char[currentMatrix.GetLength(0) + rowNumbers, currentMatrix.GetLength(1) + rowNumbers];

            for (int i = 0; i < rowNumbers; i++) {
                for (int j = 0; j < rowNumbers; j++) {
                    int blocki = i * blockSize;
                    int blockj = j * blockSize;

                    char[,] newPattern = GetNewPattern(ref currentMatrix, blocki, blockj, blockSize, inputPatterns, outputPatterns);
                    for (int p = 0; p < blockSize + 1; p++) {
                        for (int q = 0; q < blockSize + 1; q++) {
                            newMatrix[p + i * (blockSize + 1), q + j * (blockSize + 1)] = newPattern[p, q];
                        }
                    }
                }
            }

            currentMatrix = newMatrix;
        }

        private static char[,] GetNewPattern(ref char[,] currentMatrix, int blocki, int blockj, int blockSize, List<char[,]> inputPatterns, List<char[,]> outputPatterns) {
            StringBuilder blockString = new StringBuilder();
            for (int i = blocki; i < blocki + blockSize; i++) {
                for (int j = blockj; j < blockj + blockSize; j++) {
                    blockString.Append(currentMatrix[i, j]);
                }
            }

            if (patternsFound.ContainsKey(blockString.ToString())) {
                return patternsFound[blockString.ToString()];
            }

            for (int i = 0; i < inputPatterns.Count; i++) {
                if (PatternMatch(ref currentMatrix, blocki, blockj, blockSize, inputPatterns[i])) {
                    patternsFound.Add(blockString.ToString(), outputPatterns[i]);
                    return outputPatterns[i];
                }
            }

            return null;
        }

        private static bool PatternMatch(ref char[,] currentMatrix, int blocki, int blockj, int blockSize, char[,] intputPattern) {
            bool match = true;
            bool matchStraight = true;
            bool matchStraightFlipHorizontal = true;
            bool matchRot1Straight = true;
            bool matchRot1FlipHorizontal = true;
            bool matchRot2Straight = true;
            bool matchRot2FlipHorizontal = true;
            bool matchRot3Straight = true;
            bool matchRot3FlipHorizontal = true;
            for (int i = 0; i < blockSize; i++) {
                for (int j = 0; j < blockSize; j++) {
                    if (matchStraight && currentMatrix[blocki + i, blockj + j] != intputPattern[i, j]) {
                        matchStraight = false;
                    }
                    if (matchStraightFlipHorizontal && currentMatrix[blocki + i, blockj + blockSize - 1 - j] != intputPattern[i, j]) {
                        matchStraightFlipHorizontal = false;
                    }
                    if (matchRot1Straight && currentMatrix[blocki + blockSize - 1 - j, blockj + i] != intputPattern[i, j]) {
                        matchRot1Straight = false;
                    }
                    if (matchRot1FlipHorizontal && currentMatrix[blocki + j, blockj + i] != intputPattern[i, j]) {
                        matchRot1FlipHorizontal = false;
                    }
                    if (matchRot2Straight && currentMatrix[blocki + blockSize - 1 - i, blockj + blockSize - 1 - j] != intputPattern[i, j]) {
                        matchRot2Straight = false;
                    }
                    if (matchRot2FlipHorizontal && currentMatrix[blocki + blockSize - 1 - i, blockj + j] != intputPattern[i, j]) {
                        matchRot2FlipHorizontal = false;
                    }
                    if (matchRot3Straight && currentMatrix[blocki + j, blockj + blockSize - 1 - i] != intputPattern[i, j]) {
                        matchRot3Straight = false;
                    }
                    if (matchRot3FlipHorizontal && currentMatrix[blocki + blockSize - 1 - j, blockj + blockSize - 1 - i] != intputPattern[i, j]) {
                        matchRot3FlipHorizontal = false;
                    }

                    if (!matchStraight && !matchStraightFlipHorizontal 
                        && !matchRot1Straight && !matchRot1FlipHorizontal
                        && !matchRot2Straight && !matchRot2FlipHorizontal 
                        && !matchRot3Straight && !matchRot3FlipHorizontal) {
                        match = false;
                        break;
                    }
                }
            }

            return match;
        }

        private static int NumberOfPixelsOn(ref char[,] currentMatrix) {
            int numberOfPixelsOn = 0;
            for (int i = 0; i < currentMatrix.GetLength(0); i++) {
                for (int j = 0; j < currentMatrix.GetLength(1); j++) {
                    if (currentMatrix[i, j] == '#') {
                        numberOfPixelsOn++;
                    }
                }
            }

            return numberOfPixelsOn;
        }

        private static void ReadInputOutputPatterns(string line) {
            string[] lineSplit = line.Split();

            string[] inputSplit = lineSplit[0].Split(new char[] { '/' });
            char[,] inputCharMatrix = new char[inputSplit.Length, inputSplit.Length];
            for (int i = 0; i < inputCharMatrix.GetLength(0); i++) {
                char[] inputCharArray = inputSplit[i].ToCharArray();
                for (int j = 0; j < inputCharArray.Length; j++) {
                    inputCharMatrix[i, j] = inputCharArray[j];
                }
            }

            string[] outputSplit = lineSplit[2].Split(new char[] { '/' });
            char[,] outputCharMatrix = new char[outputSplit.Length, outputSplit.Length];
            for (int i = 0; i < outputCharMatrix.GetLength(0); i++) {
                char[] outputCharArray = outputSplit[i].ToCharArray();
                for (int j = 0; j < outputCharArray.Length; j++) {
                    outputCharMatrix[i, j] = outputCharArray[j];
                }
            }

            if (inputCharMatrix.GetLength(0) == 2) {
                input2x2Patterns.Add(inputCharMatrix);
                output3x3Patterns.Add(outputCharMatrix);
            } else {
                input3x3Patterns.Add(inputCharMatrix);
                output4x4Patterns.Add(outputCharMatrix);
            }
        }
    }
}