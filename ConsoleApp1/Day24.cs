﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2017 {
    class Day24
    {

        public static void Solve() {
            //StrongestBridge("..\\..\\Day24-Input Example.txt");
            //StrongestBridge("..\\..\\Day24-Input.txt");
            //LongestStrongestBridge("..\\..\\Day24-Input Example.txt");
            LongestStrongestBridge("..\\..\\Day24-Input.txt");
        }

        private static List<Component> availableComponents = new List<Component>();

        private static void StrongestBridge(string @filePath) {
            ReadInput(filePath);

            int strongestBridge = 0;
            FindStrongestBridge(ref strongestBridge, 0, new List<Component>());

            System.Diagnostics.Debug.WriteLine(strongestBridge);
        }

        private static void FindStrongestBridge(ref int currentStrength, int nextPort, List<Component> bridgeComponents) {
            int strongestSubBridge = 0;
            for (int i = 0; i < availableComponents.Count; i++) {
                Component availableComponent = availableComponents[i];
                if (!bridgeComponents.Contains(availableComponent)) {
                    if (availableComponent.portA == nextPort || availableComponent.portB == nextPort) {
                        int subBridgeNextPort = availableComponent.portA == nextPort ? availableComponent.portB : availableComponent.portA;
                        int subBridgeStrength = availableComponent.portA + availableComponent.portB;
                        List<Component> subBridgeComponents = new List<Component>(bridgeComponents);
                        subBridgeComponents.Add(availableComponent);
                        FindStrongestBridge(ref subBridgeStrength, subBridgeNextPort, subBridgeComponents);
                        if (subBridgeStrength > strongestSubBridge) {
                            strongestSubBridge = subBridgeStrength;
                        }
                    }
                }
            }

            currentStrength += strongestSubBridge;
        }

        private static void LongestStrongestBridge(string @filePath) {
            ReadInput(filePath);

            int strongestBridge = 0;
            int longestBridge = 0;
            FindStrongestLongestBridge(ref strongestBridge, ref longestBridge, 0, new List<Component>());

            System.Diagnostics.Debug.WriteLine(strongestBridge);
        }

        private static void FindStrongestLongestBridge(ref int currentStrength, ref int currentLength, int nextPort, List<Component> bridgeComponents) {
            int strongestSubBridge = 0;
            int longestSubBridge = 0;
            for (int i = 0; i < availableComponents.Count; i++) {
                Component availableComponent = availableComponents[i];
                if (!bridgeComponents.Contains(availableComponent)) {
                    if (availableComponent.portA == nextPort || availableComponent.portB == nextPort) {
                        int subBridgeNextPort = availableComponent.portA == nextPort ? availableComponent.portB : availableComponent.portA;
                        int subBridgeStrength = availableComponent.portA + availableComponent.portB;
                        int subBridgeLength = 1;
                        List<Component> subBridgeComponents = new List<Component>(bridgeComponents);
                        subBridgeComponents.Add(availableComponent);
                        FindStrongestLongestBridge(ref subBridgeStrength, ref subBridgeLength, subBridgeNextPort, subBridgeComponents);
                        if (subBridgeLength > longestSubBridge) {
                            longestSubBridge = subBridgeLength;
                            strongestSubBridge = subBridgeStrength;
                        } else if (subBridgeLength == longestSubBridge) {
                            if (subBridgeStrength > strongestSubBridge) {
                                strongestSubBridge = subBridgeStrength;
                            }
                        }
                    }
                }
            }

            currentStrength += strongestSubBridge;
            currentLength += longestSubBridge;
        }

        private static void ReadInput(string filePath) {
            StreamReader file = new StreamReader(@filePath);

            string line;
            while ((line = file.ReadLine()) != null) {
                string[] lineSplit = Regex.Matches(line, @"\d+").Cast<Match>().Select(m => m.Value).ToArray();
                int portA = int.Parse(lineSplit[0]);
                int portB = int.Parse(lineSplit[1]);
                availableComponents.Add(new Component(portA, portB));
            }
        }

        private class Component
        {
            public int portA;
            public int portB;
            
            public Component(int portA, int portB) {
                this.portA = portA;
                this.portB = portB;
            }
        }
    }
}