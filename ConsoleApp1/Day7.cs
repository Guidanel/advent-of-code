﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2017
{
    class Day7
    {
        static List<string> seenOnlyOnce = new List<string>();

        public static void Solve() {
            //System.Diagnostics.Debug.WriteLine(Bottom("..\\..\\Day7-Input.txt"));
            System.Diagnostics.Debug.WriteLine(GetCorrectedWeight("..\\..\\Day7-Input.txt"));
        }

        static string Bottom(string @filePath) {
            StreamReader file = new StreamReader(@filePath);

            string line;
            while ((line = file.ReadLine()) != null) {
                List<string> nodes = ParseLine(line);

                foreach (string node in nodes) {
                    if (!seenOnlyOnce.Contains(node)) {
                        seenOnlyOnce.Add(node);
                    } else {
                        seenOnlyOnce.Remove(node);
                    }
                }
            }

            // Only 1 item should remain in seenOnce : the bottom
            return seenOnlyOnce[0];
        }

        private static List<string> ParseLine(string line) {
            string[] lineSplit = Regex.Matches(line, @"\w+").Cast<Match>().Select(m => m.Value).ToArray();

            List<string> nodes = new List<string>();
            for (int i = 0; i < lineSplit.Length; i++) {
                if (i != 1) {
                    nodes.Add(lineSplit[i]);
                }
            }

            return nodes;
        }

        static List<Node> allNodes = new List<Node>();

        static int GetCorrectedWeight(string @filePath) {
            StreamReader file = new StreamReader(@filePath);

            string line;
            while ((line = file.ReadLine()) != null) {
                Node nodeTemp = LineToNode(line);
                ResolveNode(nodeTemp);
            }

            Node deepestNodeOfDifferentWeight = GetDeepestNodeOfDifferentWeight(GetStartNode());

            return GetCorrectedWeight(deepestNodeOfDifferentWeight);
        }

        private static Node LineToNode(string line) {
            string[] lineSplit = Regex.Matches(line, @"\w+").Cast<Match>().Select(m => m.Value).ToArray();

            Node node = new Node();
            node.name = lineSplit[0];
            node.startWeigth = node.cumulativeWeight = int.Parse(lineSplit[1]);

            for (int i = 2; i < lineSplit.Length; i++) {
                node.childTemp.Add(lineSplit[i]);
            }

            return node;
        }

        private static Node GetStartNode() {
            Node startNode = null;
            foreach (Node node in allNodes) {
                if (node.parent == null) {
                    startNode = node;
                    break;
                }
            }

            return startNode;
        }

        private static Node GetDeepestNodeOfDifferentWeight(Node parent) {
            if (parent.child.Count == 0) {
                return null;
            }
            if (parent.child.Count == 1) {
                return GetDeepestNodeOfDifferentWeight(parent.child[0]);
            }
            if (parent.child.Count == 2) {
                return null;
            }

            Node nodeOfDifferentWeight = null;

            int weight0 = parent.child[0].cumulativeWeight;
            int weight1 = parent.child[1].cumulativeWeight;
            if (weight0 != weight1) {
                // The wrong weight is on the first or second node
                if (parent.child[2].cumulativeWeight == weight0) {
                    nodeOfDifferentWeight = parent.child[1];
                } else {
                    nodeOfDifferentWeight = parent.child[0];
                }
            } else {
                // Check if the wrong weight is on an upcoming node
                int correctCumulativeWeight = weight0;
                for (int i = 2; i < parent.child.Count; i++) {
                    if (parent.child[i].cumulativeWeight != correctCumulativeWeight) {
                        nodeOfDifferentWeight = parent.child[i];
                    }
                }
            }

            if (nodeOfDifferentWeight == null) {
                return null;
            } else {
                Node nodeDeepestChildOfDifferentWeight = GetDeepestNodeOfDifferentWeight(nodeOfDifferentWeight);
                if (nodeDeepestChildOfDifferentWeight == null) {
                    return nodeOfDifferentWeight;
                } else {
                    return nodeDeepestChildOfDifferentWeight;
                }
            }
        }

        private static int GetCorrectedWeight(Node deepestNodeOfDifferentWeight) {
            int correctCumulativeWeight;
            Node parent = deepestNodeOfDifferentWeight.parent;
            if (deepestNodeOfDifferentWeight == parent.child[0]) {
                correctCumulativeWeight = parent.child[1].cumulativeWeight;
            } else {
                correctCumulativeWeight = parent.child[0].cumulativeWeight;
            }

            return deepestNodeOfDifferentWeight.startWeigth - (deepestNodeOfDifferentWeight.cumulativeWeight - correctCumulativeWeight);
        }

        private static void ResolveNode(Node nodeTemp) {
            int startWeight = nodeTemp.cumulativeWeight;

            // Check if we've seen the child before
            foreach (string childName in nodeTemp.childTemp) {
                foreach (Node node in allNodes) {
                    if (childName.Equals(node.name)) {
                        // This new node child is a node we saw before
                        node.parent = nodeTemp;
                        nodeTemp.child.Add(node);
                        nodeTemp.cumulativeWeight += node.cumulativeWeight;
                        break;
                    }
                }
            }

            // Check if we've seen this node before
            foreach (Node node in allNodes) {
                foreach (string childName in node.childTemp) {
                    if (childName.Equals(nodeTemp.name)) {
                        // This new node is a child of a node we saw before
                        nodeTemp.parent = node;
                        node.child.Add(nodeTemp);
                    }
                }
            }

            PropagateWeight(nodeTemp, nodeTemp.cumulativeWeight);

            allNodes.Add(nodeTemp);
        }

        private static void PropagateWeight(Node nodeTemp, int addedWeight) {
            if (addedWeight == 0f) {
                return;
            }

            if (nodeTemp.parent == null) {
                return;
            }

            nodeTemp.parent.cumulativeWeight += addedWeight;
            PropagateWeight(nodeTemp.parent, addedWeight);
        }
    }

    class Node
    {
        public string name;
        public int cumulativeWeight;
        public int startWeigth;
        public List<Node> child = new List<Node>();
        public List<string> childTemp = new List<string>();
        public Node parent;
    }
}