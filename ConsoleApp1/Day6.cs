﻿using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode2017
{
    class Day6
    {
        static List<int[]> banksStates;

        public static void Solve() {
            banksStates = new List<int[]>();

            //int[] input = new int[] { 0, 2, 7, 0 };
            int[] input = new int[] { 11, 11, 13, 7, 0, 15, 5, 5, 4, 4, 1, 1, 7, 1, 15, 11 };
            System.Diagnostics.Debug.WriteLine(Steps(ref input));
        }

        static int Steps(ref int[] input) {
            int index = 0;
            if (StateAlreadySeen(ref input, ref index)) {
                // Part 1
                //return banksStates.Count;
                // Part 2
                return banksStates.Count - index;
            } else {
                banksStates.Add((int[])input.Clone());
                RedistributeBlocks(ref input);

                return Steps(ref input);
            }
        }

        static void RedistributeBlocks(ref int[] input) {
            int highestBankIndex = GetHighestBankIndex(ref input);
            int blocksToRedistributeNumber = input[highestBankIndex];
            input[highestBankIndex] = 0;

            int blocksForEveryone = blocksToRedistributeNumber / input.Length;
            int blocksRemainingToDistribute = blocksToRedistributeNumber % input.Length;
            for (int i = 0; i < input.Length; i++) {
                input[i] += blocksForEveryone;
                if (i > highestBankIndex) {
                    if (i <= highestBankIndex + blocksRemainingToDistribute) {
                        input[i]++;
                    }
                } else {
                    int indexOfLastBlockToDistribute = highestBankIndex + blocksRemainingToDistribute;
                    if (indexOfLastBlockToDistribute >= input.Length) {
                        if (i < (indexOfLastBlockToDistribute + 1) % input.Length) {
                            input[i]++;
                        }
                    }
                }
            }
        }

        static int GetHighestBankIndex(ref int[] input) {
            int highestBank = input[0];
            int index = 0;
            for (int i = 1; i < input.Length; i++) {
                if (input[i] > highestBank) {
                    index = i;
                    highestBank = input[i];
                }
            }

            return index;
        }

        static bool StateAlreadySeen(ref int[] input, ref int index) {
            int length = banksStates.Count;
            for (index = 0; index < length; index++) {
                if (input.SequenceEqual<int>(banksStates[index])) {
                    return true;
                }
            }

            return false;
        }
    }
}
