﻿using System.IO;
using System.Text;

namespace AdventOfCode2017
{
    class Day10 {

		public static void Solve() {
            //HashPart1();
            HashPart2();
        }

        private static void HashPart1() {
            int currentPosition = 0;
            int skipSize = 0;
            int[] values = new int[256];
            for (int i = 0; i <= 255; i++) {
                values[i] = i;
            }

            int[] lengths = new int[] { 70, 66, 255, 2, 48, 0, 54, 48, 80, 141, 244, 254, 160, 108, 1, 41 };
            Hash(ref currentPosition, ref skipSize, ref values, ref lengths);

            System.Diagnostics.Debug.WriteLine(values[0] * values[1]);
        }

        private static void HashPart2() {
            int currentPosition = 0;
            int skipSize = 0;
            int[] values = new int[256];
            for (int i = 0; i <= 255; i++) {
                values[i] = i;
            }

            int[] lengths = StringToIntArray("..\\..\\Day10-Input.txt");
            for (int i = 0; i < 64; i++) {
                Hash(ref currentPosition, ref skipSize, ref values, ref lengths);
            }

            StringBuilder hashBuilder = new StringBuilder();
            for (int i = 0; i < 16; i++) {
                int denseValue = 0;
                for (int j = 0; j < 16; j++) {
                    denseValue ^= values[i * 16 + j];
                }
                string hexValue = denseValue.ToString("X");
                if (hexValue.Length == 1) {
                    hashBuilder.Append('0');
                }
                hashBuilder.Append(hexValue);
            }

            System.Diagnostics.Debug.WriteLine(hashBuilder.ToString());
        }

        private static void Hash(ref int currentPosition, ref int skipSize, ref int[] values, ref int[] lengths) {
            for (int i = 0; i < lengths.Length; i++) {
                int length = lengths[i];
                if (length > 1) {
                    for (int j = currentPosition; j < currentPosition + length / 2; j++) {
                        int currentIndex = j % values.Length;
                        int swapIndex = (currentPosition + length - 1 - (j - currentPosition)) % values.Length;
                        int temp = values[swapIndex];
                        values[swapIndex] = values[currentIndex];
                        values[currentIndex] = temp;
                    }
                }

                currentPosition += length + skipSize;
                currentPosition = currentPosition % values.Length;

                skipSize++;
            }
        }

        private static int[] StringToIntArray(string @filePath) {
            StreamReader file = new StreamReader(@filePath);
            string line = file.ReadLine();

            int[] values = new int[line.Length + 5];
            for (int i = 0; i < line.Length; i++) {
                values[i] = line[i];
            }
            values[values.Length - 1 - 4] = 17;
            values[values.Length - 1 - 3] = 31;
            values[values.Length - 1 - 2] = 73;
            values[values.Length - 1 - 1] = 47;
            values[values.Length - 1 - 0] = 23;

            return values;
        }
    }
}