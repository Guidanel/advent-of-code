﻿using System.Collections.Generic;

namespace AdventOfCode2017
{
    class Day17
    {

        public static void Solve() {
            //SpinLock1(363);
            SpinLock2(363);
        }

        private static void SpinLock1(int steps) {
            List<int> values = new List<int>();
            values.Add(0);
            int currentIndex = 0;
            for (int nextValue = 1; nextValue < 2018; nextValue++) {
                int nextIndex = 1 + (currentIndex + steps) % nextValue;
                values.Insert(nextIndex, nextValue);
                currentIndex = nextIndex;
            }

            System.Diagnostics.Debug.WriteLine(values[currentIndex + 1]);
        }

        private static void SpinLock2(int steps) {
            int currentIndex = 0;
            int valueAfterZero = 0;
            for (int nextValue = 1; nextValue < 50000001; nextValue++) {
                currentIndex = 1 + (currentIndex + steps) % nextValue;
                if (currentIndex == 1) {
                    valueAfterZero = nextValue;
                }
            }

            System.Diagnostics.Debug.WriteLine(valueAfterZero);
        }
    }
}