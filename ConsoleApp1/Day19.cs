﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace AdventOfCode2017 {
	class Day19 {

		public static void Solve() {
			//LettersOnPath("..\\..\\Day19-Input Example.txt");
			LettersOnPath("..\\..\\Day19-Input.txt");
		}

		private static List<char[]> path = new List<char[]>();
		private static int pathWorldWidth, pathWorldHeight;
		private static StringBuilder letters = new StringBuilder();
		private static List<Direction> horizontalDirections = new List<Direction> { Direction.Left, Direction.Right };
		private static List<Direction> verticalDirections = new List<Direction> { Direction.Up, Direction.Down };
		private static int stepsNumber = 1;

		private static void LettersOnPath(string @filePath) {
			StreamReader file = new StreamReader(@filePath);

			string line;

			// Read the first line and retrieve the start column at the same time
			line = file.ReadLine();
			path.Add(line.ToCharArray());
			int startColumn = line.IndexOf('|');
			int startRow = 0;

			pathWorldWidth = line.Length;

			while ((line = file.ReadLine()) != null) {
				path.Add(line.ToCharArray());
			}

			pathWorldHeight = path.Count;

			FollowPath(Direction.Down, startRow, startColumn);

			System.Diagnostics.Debug.WriteLine(letters.ToString());
			System.Diagnostics.Debug.WriteLine(stepsNumber);
		}

		private static void FollowPath(Direction direction, int startRow, int startColumn) {
			int i = startRow;
			int j = startColumn;
			while (MovedOneStep(ref direction, ref i, ref j)) {
			}
		}

		private static bool MovedOneStep(ref Direction direction, ref int i, ref int j) {
			char currentChar = path[i][j];
			if (IsLetter(currentChar)) {
				letters.Append(currentChar);
			}

			int old_i = i;
			int old_j = j;
			UpdateIndexes(ref direction, ref i, ref j);

			bool moved = old_i != i || old_j != j;
			return moved;
		}

		private static void UpdateIndexes(ref Direction direction, ref int i, ref int j) {
			if (!TryMove(direction, ref i, ref j)) {
				List<Direction> otherDirections = horizontalDirections.Contains(direction) ? verticalDirections : horizontalDirections;
				foreach (Direction otherDirection in otherDirections) {
					direction = otherDirection;
					if (TryMove(direction, ref i, ref j)) {
						break;
					}
				}
			}
		}

		private static bool TryMove(Direction direction, ref int i, ref int j) {
			bool canMove = false;
			switch (direction) {
				case Direction.Up:
					if (i > 0 && path[i - 1][j] != ' ') {
						i--;
						canMove = true;
					}
					break;
				case Direction.Right:
					if (j < pathWorldWidth - 1 && path[i][j + 1] != ' ') {
						j++;
						canMove = true;
					}
					break;
				case Direction.Down:
					if (i < pathWorldHeight - 1 && path[i + 1][j] != ' ') {
						i++;
						canMove = true;
					}
					break;
				case Direction.Left:
					if (j > 0 && path[i][j - 1] != ' ') {
						j--;
						canMove = true;
					}
					break;
			}

			if (canMove) {
				stepsNumber++;
			}

			return canMove;
		}

		private static bool IsLetter(char character) {
			return Regex.Match(character.ToString(), @"\w").Success;
		}

		private enum Direction {
			Up,
			Right,
			Down,
			Left
		}
	}
}