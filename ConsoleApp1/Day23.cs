﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2017 {
	class Day23 {

		public static void Solve() {
            //MulCount("..\\..\\Day23-Input.txt");
            ProcessOptimized();
        }

        private static void ProcessOptimized() {
            int b = 79 * 100 + 100000;
            int c = b + 17000;
            int h = 0;
            while (b <= c) {
                if (!IsPrime(b)) {
                    h++;
                }
                b += 17;
            }

            System.Diagnostics.Debug.WriteLine(h);
        }

        private static int mulCount;

		private static void MulCount(string @filePath) {
			List<Instruction> instructions = RetrieveInstructions(filePath);

			Dictionary<string, long> registers = new Dictionary<string, long>();

            int i = 0;
			while (i < instructions.Count) {
                int indexOffset = ExecuteInstruction(instructions[i], registers);
				i += indexOffset;
			}
            
            System.Diagnostics.Debug.WriteLine(mulCount);
        }

        private static int ExecuteInstruction(Instruction instruction, Dictionary<string, long> registers) {
			int indexOffset = 1;
			switch (instruction.command) {
				
				case "set":
					SetValue(instruction.register, GetValue(instruction.value, registers), registers);
                    break;
				case "sub":
					SetValue(instruction.register, GetValue(instruction.register, registers) - GetValue(instruction.value, registers), registers);
					break;
				case "mul":
					SetValue(instruction.register, GetValue(instruction.register, registers) * GetValue(instruction.value, registers), registers);
                    mulCount++;
                    break;
				case "jnz":
					if (GetValue(instruction.register, registers) != 0) {
						indexOffset = (int)GetValue(instruction.value, registers);
                    }
					break;
			}

			return indexOffset;
		}

		private static void SetValue(string register, long value, Dictionary<string, long> registers) {
			if (registers.ContainsKey(register)) {
				registers[register] = value;
			} else {
				registers.Add(register, value);
			}
		}

		private static long GetValue(string registerOrValue, Dictionary<string, long> registers) {
			long value;
			if (!long.TryParse(registerOrValue, out value)) {
				if (registers.ContainsKey(registerOrValue)) {
					value = registers[registerOrValue];
				} else {
					value = 0;
					registers.Add(registerOrValue, 0);
				}
			}

			return value;
		}

		private static List<Instruction> RetrieveInstructions(string filePath) {
			StreamReader file = new StreamReader(@filePath);

			List<Instruction> instructions = new List<Instruction>();

			string line;
			while ((line = file.ReadLine()) != null) {
				string[] lineSplit = line.Split();
				Instruction instruction = new Instruction();
				instruction.command = lineSplit[0];
				instruction.register = lineSplit[1];
				if (lineSplit.Length > 2) {
					instruction.value = lineSplit[2];
				}
				instructions.Add(instruction);
			}

			return instructions;
		}

		private struct Instruction {
			public string command;
			public string register;
			public string value;
		}

        private static bool IsPrime(int number) {
            if (number == 1) return false;
            if (number == 2) return true;
            if (number % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(number));

            for (int i = 3; i <= boundary; i += 2) {
                if (number % i == 0) return false;
            }

            return true;
        }
    }
}