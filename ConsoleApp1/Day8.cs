﻿using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2017
{
    class Day8 {

		public static void Solve() {
			//System.Diagnostics.Debug.WriteLine(LargestRegisterValue("..\\..\\Day8-Input Example.txt"));
			System.Diagnostics.Debug.WriteLine(LargestRegisterValue("..\\..\\Day8-Input.txt"));
		}

		private static Dictionary<string, int> registerValues = new Dictionary<string, int>();
		private static int highestValue = 0;

		private static int LargestRegisterValue(string @filePath) {
			StreamReader file = new StreamReader(@filePath);

			string line;
			while ((line = file.ReadLine()) != null) {
				UpdateRegisters(line);
			}

			int maxValue = int.MinValue;
			foreach (var register in registerValues) {
				if (register.Value > maxValue) {
					maxValue = register.Value;
				}
			}

			return maxValue;
		}

		private static void UpdateRegisters(string line) {
			string[] lineSplit = line.Split();

			int comparedValue = int.Parse(lineSplit[lineSplit.Length - 1]);
			string comparator = lineSplit[lineSplit.Length - 2];
			string comparedRegister = lineSplit[lineSplit.Length - 3];

			if (!registerValues.ContainsKey(comparedRegister)) {
				registerValues.Add(comparedRegister, 0);
			}

			bool isConditionVerified = false;
			switch (comparator) {
				case ">":
					isConditionVerified = registerValues[comparedRegister] > comparedValue;
					break;
				case ">=":
					isConditionVerified = registerValues[comparedRegister] >= comparedValue;
					break;
				case "<":
					isConditionVerified = registerValues[comparedRegister] < comparedValue;
					break;
				case "<=":
					isConditionVerified = registerValues[comparedRegister] <= comparedValue;
					break;
				case "==":
					isConditionVerified = registerValues[comparedRegister] == comparedValue;
					break;
				case "!=":
					isConditionVerified = registerValues[comparedRegister] != comparedValue;
					break;
			}

			if (isConditionVerified) {
				string targetRegister = lineSplit[0];
				if (!registerValues.ContainsKey(targetRegister)) {
					registerValues.Add(targetRegister, 0);
				}

				string instruction = lineSplit[1];
				int modifier = int.Parse(lineSplit[2]);
				if (instruction.Equals("inc")) {
					registerValues[targetRegister] += modifier;
				} else {
					registerValues[targetRegister] -= modifier;
				}

				if (registerValues[targetRegister] > highestValue) {
					highestValue = registerValues[targetRegister];
				}
			}
		}
	}
}