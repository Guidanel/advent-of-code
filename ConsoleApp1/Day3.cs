﻿using System.Collections.Generic;

namespace AdventOfCode2017
{
    class Day3
    {
        public static void Solve() {
            int input = 277678;
            System.Diagnostics.Debug.WriteLine(Distance(input));
            System.Diagnostics.Debug.WriteLine(Value(input));
        }

        static int Distance(int input) {
            int distance = 0;
            
            if (input == 1) {
                return 0;
            }

            int loopStart = 2;
            int loopEnd = 9;

            while (true) {
                if (input >= loopStart && input <= loopEnd) {
                    break;
                } else {
                    // Try next loop
                    int previousLoopLength = loopEnd - loopStart + 1;
                    loopStart = loopEnd + 1;
                    loopEnd = loopStart - 1 + previousLoopLength + 8;
                }
            }

            int loopLength = loopEnd - loopStart + 1;
            int loopNumber = loopLength / 8;
            int distanceCycleLength = loopLength / 4;
            int positionInCycle = (input - loopStart) % distanceCycleLength;
            if (positionInCycle + 1 <= distanceCycleLength / 2) {
                // First half of the distance cycle
                int distanceCycleStartValue = loopNumber * 2 - 1;
                distance = distanceCycleStartValue - positionInCycle;
            } else {
                // Second half of the distance cycle
                int distanceCycleEndValue = loopNumber * 2;
                distance = distanceCycleEndValue - (distanceCycleLength - (positionInCycle + 1));
            }

            return distance;
        }

        static int Value(int input) {

            if (input == 1) {
                return 0;
            }

            List<int> values = new List<int>();
            values.Add(0);
            values.Add(1);
            values.Add(1);
            values.Add(2);
            values.Add(4);
            values.Add(5);
            values.Add(10);
            values.Add(11);
            values.Add(23);
            values.Add(25);

            int index = 10;
            int loopStart = 10, previousLoopStart = 2;
            int loopEnd = 25;
            int loopLength = 16;
            int borderLength = loopLength / 4;

            while (values[index - 1] < input) {
                int value = values[index - 1];
                if (index == loopStart) {
                    // Loop start
                    if (index > 2) {
                        value += values[previousLoopStart];
                    }
                } else if (index == loopStart + 1) {
                    // Just after the loop start
                    value += values[loopStart - 1] + values[previousLoopStart] + values[previousLoopStart + 1];
                } else if (index == loopEnd) {
                    // Loop end
                    value += values[loopStart] + values[loopStart - 1];
                    previousLoopStart = loopStart;
                    loopStart = loopEnd + 1;
                    loopLength += 8;
                    loopEnd = loopStart + loopLength - 1;
                    borderLength = loopLength / 4;
                } else if (index == loopEnd - 1) {
                    // Just before the loop end
                    value += values[loopStart] + values[loopStart - 1] + values[loopStart - 2];
                } else {
                    int indexFromLastCorner = (index - loopStart + 1) % borderLength;
                    int previousBorderLength = (loopLength - 8) / 4;
                    if (indexFromLastCorner == 0) {
                        // Corner
                        int cornerNumber = (index - loopStart + 1) / borderLength;
                        value += values[previousLoopStart + previousBorderLength * cornerNumber - 1];
                    } else {
                        int borderNumber = (index - indexFromLastCorner - loopStart + 1) / borderLength;
                        if (indexFromLastCorner == borderLength - 1) {
                            // Just before the corner
                            int previousNearValueIndex = previousLoopStart + borderNumber * previousBorderLength - 1 + indexFromLastCorner - 1;
                            value += values[previousNearValueIndex] + values[previousNearValueIndex - 1];
                        } else if (indexFromLastCorner == 1) {
                            // Just after the corner
                            int previousNearCornerIndex = previousLoopStart + borderNumber * previousBorderLength - 1;
                            value += values[index - 2] + values[previousNearCornerIndex] + values[previousNearCornerIndex + 1];
                        } else {
                            int previousNearValueIndex = previousLoopStart + borderNumber * previousBorderLength - 1 + indexFromLastCorner - 1;
                            value += values[previousNearValueIndex + 1] + values[previousNearValueIndex] + values[previousNearValueIndex - 1];
                        }
                    }
                }

                values.Add(value);
                index++;
            }

            return values[index - 1];
        }
    }
}
