﻿namespace AdventOfCode2017
{
    class Day14
    {

        public static void Solve() {
			//BlocksUsed();
			GroupsNumber();
		}

		private static byte[][] bytesArray = new byte[128][];

		private static void GroupsNumber() {
			for (int i = 0; i < 128; i++) {
				bytesArray[i] = ByteHash("ffayrhll-" + i);
			}

			int groupsNumber = 0;
			for (int i = 0; i < 128; i++) {
				for (int j = 0; j < 128; j++) {
					if (IsBitOne(i, j)) {
						VisitGroup(i, j);
						groupsNumber++;
					}
				}
			}

			System.Diagnostics.Debug.WriteLine(groupsNumber);
		}

		private static void VisitGroup(int i, int j) {
			if (!IsBitOne(i, j)) {
				return;
			}

			SetBitZero(i, j);

			if (i > 0) {
				VisitGroup(i - 1, j);
			}
			if (i < 127) {
				VisitGroup(i + 1, j);
			}
			if (j > 0) {
				VisitGroup(i, j - 1);
			}
			if (j < 127) {
				VisitGroup(i, j + 1);
			}
		}

		private static bool IsBitOne(int i, int j) {
			byte targetByte = bytesArray[i][j / 8];
			byte mask = (byte) (128 >> (j % 8));
			return (targetByte & mask) == mask;
		}

		private static void SetBitZero(int i, int j) {
			byte targetByte = bytesArray[i][j / 8];
			byte mask = (byte) (128 >> (j % 8));
			bytesArray[i][j / 8] = (byte) (targetByte ^ mask);
		}

		private static void BlocksUsed() {
            int blocksUsed = 0;
            for (int i = 0; i < 128; i++) {
                blocksUsed += GetBlocksUsedNumber(ByteHash("ffayrhll-" + i));
            }

            System.Diagnostics.Debug.WriteLine(blocksUsed);
        }

        private static int GetBlocksUsedNumber(byte[] hash) {
            int blocksUsed = 0;
            for (int i = 0; i < hash.Length; i++) {
                blocksUsed += BitSets(hash[i]);
            }

            return blocksUsed;
        }

        private static byte[] ByteHash(string input) {
			byte[] hash = new byte[16];
            int currentPosition = 0;
            int skipSize = 0;
            int[] values = new int[256];
            for (int i = 0; i <= 255; i++) {
                values[i] = i;
            }

            int[] lengths = StringToIntArray(input);
            for (int i = 0; i < 64; i++) {
                Hash(ref currentPosition, ref skipSize, ref values, ref lengths);
            }
			
            for (int i = 0; i < 16; i++) {
                int denseValue = 0;
                for (int j = 0; j < 16; j++) {
                    denseValue ^= values[i * 16 + j];
                }
				hash[i] = (byte)denseValue;
			}

            return hash;
        }

        private static void Hash(ref int currentPosition, ref int skipSize, ref int[] values, ref int[] lengths) {
            for (int i = 0; i < lengths.Length; i++) {
                int length = lengths[i];
                if (length > 1) {
                    for (int j = currentPosition; j < currentPosition + length / 2; j++) {
                        int currentIndex = j % values.Length;
                        int swapIndex = (currentPosition + length - 1 - (j - currentPosition)) % values.Length;
                        int temp = values[swapIndex];
                        values[swapIndex] = values[currentIndex];
                        values[currentIndex] = temp;
                    }
                }

                currentPosition += length + skipSize;
                currentPosition = currentPosition % values.Length;

                skipSize++;
            }
        }

        private static int[] StringToIntArray(string input) {
            int[] values = new int[input.Length + 5];
            for (int i = 0; i < input.Length; i++) {
                values[i] = input[i];
            }
            values[values.Length - 1 - 4] = 17;
            values[values.Length - 1 - 3] = 31;
            values[values.Length - 1 - 2] = 73;
            values[values.Length - 1 - 1] = 47;
            values[values.Length - 1 - 0] = 23;

            return values;
        }

        private static int BitSets(byte value) {
            int count = 0;
            while (value != 0) {
                if ((value & 0x1) == 0x1) {
                    count++;
                }
                value >>= 1;
            }

            return count;
        }
    }
}