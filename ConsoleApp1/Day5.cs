﻿using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2017
{
    class Day5
    {
        public static void Solve() {
            System.Diagnostics.Debug.WriteLine(Steps("..\\..\\Day5-Input.txt"));
        }

        static int Steps(string @filePath) {
            StreamReader file = new StreamReader(@filePath);

            string line;
            List<int> values = new List<int>();
            while ((line = file.ReadLine()) != null) {
                values.Add(int.Parse(line));
            }

            int steps = 0;
            int index = 0;
            while (index >= 0 && index < values.Count) {
                steps++;
                int indexSave = index;
                index += values[indexSave];

                // Part 1
                //values[indexSave]++;

                // Part 2
                if (values[indexSave] < 3) {
                    values[indexSave]++;
                } else {
                    values[indexSave]--;
                }
            }

            return steps;
        }
    }
}
